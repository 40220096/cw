package me.caedus.Manager;

import java.util.HashMap;
import java.util.UUID;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;

public class GameManager {
	
	private final static GameManager INSTANCE = new GameManager();
	private HashMap<UUID, CWPlayer> p2cwp = new HashMap<UUID, CWPlayer>();
	
	private CWTeam red = new CWTeam("Red");
	private CWTeam blue = new CWTeam("Blue");
	private CWTeam white = new CWTeam("White");
	private CWTeam black = new CWTeam("Black");
	
	public GameManager(){
		
	}
	
	public HashMap<UUID, CWPlayer> getPlayerToCWPlayer(){
		return p2cwp;
	}
	public static GameManager getInstance(){
		return INSTANCE;
	}
	public CWTeam getRedTeam(){
		return red;
	}
	public CWTeam getBlueTeam(){
		return blue;
	}
	public CWTeam getWhiteTeam(){
		return white;
	}
	public CWTeam getBlackTeam(){
		return black;
	}
}
