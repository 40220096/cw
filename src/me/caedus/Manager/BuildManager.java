package me.caedus.Manager;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.World;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.world.DataException;

import me.caedus.ColonyWars.CWTeam;
import me.caedus.ColonyWars.ColonyWars;
import me.caedus.ColonyWarsBuildings.CWBuildType;

@SuppressWarnings("deprecation")
public class BuildManager {
	private final static BuildManager INSTANCE = new BuildManager();
	public static BuildManager getInstance(){
		return INSTANCE;	
	}
	private ColonyWars plugin = ColonyWars.getInstance();
	LocationManager lm = LocationManager.getInstance();
	private World world = plugin.getServer().getWorld("World");
	
	public void build(CWTeam cwt, CWBuildType cwbt) throws MaxChangedBlocksException, DataException, IOException{
		if(cwt.getColour().equalsIgnoreCase("Blue")){
			if(cwbt == CWBuildType.TOWN_HALL){
				//Spawn Town Hall Schematic
				File file = new File(plugin.getDataFolder().getPath()+"//BlueSchem//town_hall.schematic");
				Vector vector = BukkitUtil.toVector(lm.getBlueTownHall());
				
				EditSession es = new EditSession(new BukkitWorld(world), 999999999);
			    CuboidClipboard cc = CuboidClipboard.loadSchematic(file);
			    cc.paste(es, vector, false);
			}
			if(cwbt == CWBuildType.FARM){
				//Spawn Farm Schematic
				spawnBuilding(lm.getBlueFarm(), new File(plugin.getDataFolder().getPath()+"//BlueSchem//farm.schematic"));
			}
		}
	}
	
	
	public long getCost(CWBuildType cwbt){
		long cost = 0;
		if(cwbt == CWBuildType.FARM)
			cost = 1000;
		if(cwbt == CWBuildType.CHURCH)
			cost = 1500;
		
		
		return cost;
	}
	
	private void spawnBuilding(Location loc, File file) throws DataException, IOException, MaxChangedBlocksException{
		Vector vector = BukkitUtil.toVector(loc);
		
		EditSession es = new EditSession(new BukkitWorld(world), 999999999);
	    CuboidClipboard cc = CuboidClipboard.loadSchematic(file);
	    cc.paste(es, vector, false);
	}
	
}
