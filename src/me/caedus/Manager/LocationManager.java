package me.caedus.Manager;

import org.bukkit.Location;
import org.bukkit.World;

import me.caedus.ColonyWars.ColonyWars;

public class LocationManager {
	private final static LocationManager INSTANCE = new LocationManager();
	public static LocationManager getInstance(){
		return INSTANCE;
	}
	private World world = ColonyWars.getInstance().getServer().getWorld("World");
	
	//Blue Team Building Locations
	Location blueTownHall = new Location(world, 183, 94, 200);
	Location blueFarm = new Location(world, 150, 94, 200);
	
	public Location getBlueTownHall(){
		return this.blueTownHall;
	}
	public Location getBlueFarm(){
		return this.blueFarm;
	}
	

}
