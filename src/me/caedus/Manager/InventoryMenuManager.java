package me.caedus.Manager;

import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;
import me.caedus.ColonyWars.ColonyWars;
import me.caedus.ColonyWarsBuildings.CWBuildType;
import me.caedus.ColonyWarsBuildings.CWBuilding;

public class InventoryMenuManager {
	
	private final static InventoryMenuManager INSTANCE = new InventoryMenuManager();
	public static InventoryMenuManager getInstance(){
		return INSTANCE;
	}
	
	@SuppressWarnings("deprecation")
	public void procInv(CWBuilding cwb, CWPlayer cwp){
		ItemStack rg = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());
		ItemStack gg = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData());
		ItemMeta rgim = rg.getItemMeta();
		rgim.setDisplayName(" ");
		rg.setItemMeta(rgim);
		ItemMeta ggim = gg.getItemMeta();
		ggim.setDisplayName(" ");
		gg.setItemMeta(ggim);
		
		
		CWTeam cwt = cwp.getTeam();
		String buildingName = proc(cwb.getCWBuildType());
		
		//Inventory setup
		Inventory inv = ColonyWars.getInstance().getServer().createInventory(null, 27, buildingName);
		//" Team "+buildingName+"(Rank "+cwb.getRank()+")
		ItemStack teamInfo = new ItemStack(Material.BOOK, 1);
		ItemMeta tmim = teamInfo.getItemMeta();
		ArrayList<String> tma = new ArrayList<String>();
		tma.add(ChatColor.GREEN+"Team Balance: "+ChatColor.GOLD+cwt.getTeamBalance());
		tma.add(ChatColor.GREEN+"Team Members: "+ChatColor.GOLD+cwt.getCWPlayers().size());
		tma.add(ChatColor.GREEN+"Your Balance: "+ChatColor.GOLD+cwp.getBalance());
		tmim.setLore(tma);
		
		
		if(cwb.getCWBuildType() == CWBuildType.TOWN_HALL){
			tmim.setDisplayName(cwt.getColour()+" Team Info");
			
			//Building Items
			//Farm
			ItemStack frm = new ItemStack(Material.WHEAT, 1);
			ItemMeta fim = frm.getItemMeta();
			ArrayList<String> fl = new ArrayList<String>();
			
			if(!cwt.hasFarm()){
				fim.setDisplayName(ChatColor.RED+"Farm");
				
				long cost = BuildManager.getInstance().getCost(CWBuildType.FARM);
				
				if(cwt.getTeamBalance() < cost){
					fl.add(ChatColor.GREEN+"Build Cost: "+ChatColor.RED+cwt.getTeamBalance()+"/"+ChatColor.GREEN+cost);
				} else {
					fl.add(ChatColor.GREEN+"Build Cost: "+cwt.getTeamBalance()+"/"+ChatColor.GREEN+cost);		
				}	
			} else if(cwt.hasFarm() && cwt.getFarm() != null){
				CWBuilding farm = cwt.getFarm();
				fim.setDisplayName(ChatColor.GREEN+"Farm");
				fl.add(ChatColor.GREEN+"Rank: "+ChatColor.GOLD+farm.getRank());
				fl.add("Upgrade Cost: tba");
			}
			fim.setLore(fl);
			frm.setItemMeta(fim);
			
			inv.setItem(1, rg);
			inv.setItem(2, gg);
			inv.setItem(3, rg);
			inv.setItem(4, gg);
			inv.setItem(5, rg);
			inv.setItem(6, gg);
			inv.setItem(7, rg);
			inv.setItem(8, gg);
			inv.setItem(9, gg);
			inv.setItem(10, rg);
			inv.setItem(11, frm);
			inv.setItem(12, frm);
			inv.setItem(13, frm);
			inv.setItem(14, frm);
			inv.setItem(15, frm);
			inv.setItem(16, rg);
			inv.setItem(17, gg);
			inv.setItem(18, gg);
			inv.setItem(19, rg);
			inv.setItem(20, gg);
			inv.setItem(21, rg);
			inv.setItem(22, gg);
			inv.setItem(23, rg);
			inv.setItem(24, gg);
			inv.setItem(25, rg);
			inv.setItem(26, gg);
		}
		
		
		teamInfo.setItemMeta(tmim);
		inv.setItem(0, teamInfo);
		cwp.getPlayer().openInventory(inv);
	}
	
	private String proc(CWBuildType cwbt){
		String s;
		String newS = "";
		s = cwbt.toString().toLowerCase();
		String[] ss = s.split("_");
		for(String str : ss){
			str = str.substring(0, 1).toUpperCase()+str.substring(1).toLowerCase()+" ";
			newS = newS+str;
		}
		if(newS.substring(newS.length() -1).equals(" "))
			newS = newS.substring(0, newS.length() -1);
		return newS;
	}
}
