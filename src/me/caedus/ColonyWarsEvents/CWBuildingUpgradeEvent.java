package me.caedus.ColonyWarsEvents;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.caedus.ColonyWarsBuildings.CWBuilding;

public class CWBuildingUpgradeEvent extends Event implements Cancellable {
	private CWBuilding building;
	private boolean isCancelled = false;
	private int previousRank;
	private int newRank;
	
	private static final HandlerList handlers = new HandlerList();
	
	public CWBuildingUpgradeEvent(CWBuilding b, int previousRank, int newRank){
		this.building = b;
		this.previousRank = previousRank;
		this.newRank = newRank;
	}
	
	public CWBuilding getCWBuilding(){
		return this.building;
	}
	public int getPreviousRank(){
		return this.previousRank;
	}
	public int getNewRank(){
		return this.newRank;
	}

	//Default code for events
	@Override
	public boolean isCancelled() {
		return this.isCancelled;
	}

	@Override
	public void setCancelled(boolean isC) {
		this.isCancelled = isC;
	}
	 
	@Override
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
