package me.caedus.ColonyWarsEvents;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;

public class CWPlayerJoinTeamEvent extends Event implements Cancellable {

	private CWPlayer cwp;
	private CWTeam team;
	private boolean isCancelled = false;
	
	private static final HandlerList handlers = new HandlerList();
	
	public CWPlayerJoinTeamEvent(CWPlayer cwp, CWTeam team){
		this.cwp = cwp;
		this.team = team;
	}
	
	public CWPlayer getCWPlayer(){
		return this.cwp;
	}
	public CWTeam getTeam(){
		return this.team;
	}

	//Default code for events
	@Override
	public boolean isCancelled() {
		return this.isCancelled;
	}

	@Override
	public void setCancelled(boolean isC) {
		this.isCancelled = isC;
	}
	 
	@Override
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
