package me.caedus.ColonyWarsEvents;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.caedus.ColonyWarsBuildings.CWBuilding;

public class CWBuildingBuildEvent extends Event implements Cancellable {
	private CWBuilding building;
	private boolean isCancelled = false;
	
	private static final HandlerList handlers = new HandlerList();
	
	public CWBuildingBuildEvent(CWBuilding b){
		this.building = b;
	}
	
	public CWBuilding getCWBuilding(){
		return this.building;
	}

	//Default code for events
	@Override
	public boolean isCancelled() {
		return this.isCancelled;
	}

	@Override
	public void setCancelled(boolean isC) {
		this.isCancelled = isC;
	}
	 
	@Override
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}
}
