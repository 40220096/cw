package me.caedus.ColonyWarsEventsListener;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.world.DataException;

import me.caedus.ColonyWarsBuildings.CWBuildType;
import me.caedus.ColonyWarsBuildings.CWBuilding;
import me.caedus.ColonyWarsEvents.CWBuildingBuildEvent;

public class CWBuildingBuildEventListener implements Listener {
	
	@EventHandler
	public void onCWBuildingBuild(CWBuildingBuildEvent e) throws MaxChangedBlocksException, DataException, IOException{
		CWBuilding cwb = e.getCWBuilding();
		if(cwb.getCWBuildType() == CWBuildType.TOWN_HALL){
			cwb.getTeam().build(cwb.getCWBuildType());
			cwb.getTeam().setTownHall(cwb);
			}
		if(cwb.getCWBuildType() == CWBuildType.FARM){
			cwb.getTeam().build(cwb.getCWBuildType());
			cwb.getTeam().setFarm(cwb);
		}
		
		
		
		Bukkit.broadcastMessage("event fired properly "+cwb.getCWBuildType());
	}

}
