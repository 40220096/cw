package me.caedus.EventListeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;
import me.caedus.ColonyWars.ColonyWars;
import me.caedus.ColonyWarsBuildings.CWBuildType;
import me.caedus.ColonyWarsBuildings.CWBuilding;
import me.caedus.ColonyWarsEvents.CWBuildingBuildEvent;
import me.caedus.ColonyWarsEvents.CWBuildingUpgradeEvent;
import me.caedus.Manager.BuildManager;
import me.caedus.Manager.GameManager;
import me.caedus.Manager.LocationManager;

public class InventoryClickEventListener implements Listener {
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		
		ColonyWars cw = ColonyWars.getInstance();
		
		Player p = (Player) e.getWhoClicked();
		CWPlayer cwp = GameManager.getInstance().getPlayerToCWPlayer().get(p.getUniqueId());
		if(e.getInventory().getName().equalsIgnoreCase("Town Hall")){
			//if(e.getInventory().getTitle().contains(cwp.getTeam().getColour()+" Team")){
			//Checking if the building is Town Hall
			if(e.getInventory().getName().contains("Town Hall")){
				if(e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR){
					e.setCancelled(true);
					
					CWTeam cwt = cwp.getTeam();
					
					Material m = e.getCurrentItem().getType();
					if(m == Material.WHEAT){
						BuildManager bm = BuildManager.getInstance();
						LocationManager lm = LocationManager.getInstance();
						if(cwt.hasFarm()){
							//Means it needs to be upgraded, need to add upgrade cost check
							CWBuilding cwb = cwt.getFarm();
							CWBuildingUpgradeEvent ev = new CWBuildingUpgradeEvent(cwb, cwb.getRank(), cwb.getRank()+1);
							cw.getServer().getPluginManager().callEvent(ev);
							
							if(!ev.isCancelled()){
								cwb.setRank(cwb.getRank()+1);
								for(CWPlayer cwpp : cwt.getCWPlayers()){
									cwpp.getPlayer().sendMessage(cwt.getColour()+"Team Farm has been upgraded to Rank "+cwb.getRank());
								}
							}
							
						} else if(!cwt.hasFarm()){
							long buyCost = bm.getCost(CWBuildType.FARM);
							//Means it's being purchased
							if(cwt.getTeamBalance() > buyCost){
								CWBuilding cwb = new CWBuilding(CWBuildType.FARM, lm.getBlueFarm(),cwt);
								CWBuildingBuildEvent ev = new CWBuildingBuildEvent(cwb);
								cw.getServer().getPluginManager().callEvent(ev);
								
								if(!ev.isCancelled()){
									cwt.setTeamBalance(cwt.getTeamBalance() - buyCost);
									for(CWPlayer cwpp : cwt.getCWPlayers()){
										cwpp.getPlayer().sendMessage("Blue Team Farm was successfully built.");
									}
								}
							} else {
								cwp.getPlayer().sendMessage("Sorry, you cannot afford this building currently. "+cwt.getTeamBalance()+"/"+buyCost+".");
							}
						}
						e.getWhoClicked().closeInventory();
					}
					
					
					
				}
			}
			
			
			
		}
		
	}

}
