package me.caedus.EventListeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

public class BlockBreakEventListener implements Listener {
	
	@EventHandler
	public void onBlockBreakEvent(BlockBreakEvent e){
		//Player p = (Player) e.getPlayer();
		Material m = e.getBlock().getType();		
		if(m == Material.GOLD_ORE){
			e.getBlock().getWorld().getBlockAt(e.getBlock().getLocation()).setType(Material.AIR);
			e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT, 4));
		} else if (m == Material.LOG || m == Material.LOG_2){
			e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new ItemStack(Material.GOLD_INGOT, 2));
			e.getBlock().getWorld().getBlockAt(e.getBlock().getLocation()).setType(Material.AIR);
		} else {
			e.getBlock().getWorld().getBlockAt(e.getBlock().getLocation()).setType(Material.AIR);
		}	
	}
}
