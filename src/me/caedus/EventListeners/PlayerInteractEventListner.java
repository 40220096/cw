	package me.caedus.EventListeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;
import me.caedus.ColonyWarsBuildings.CWBuilding;
import me.caedus.Manager.GameManager;
import me.caedus.Manager.InventoryMenuManager;

public class PlayerInteractEventListner implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		Player p = (Player) e.getPlayer();
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
			Material m = e.getClickedBlock().getType();
			if(m == Material.WOOL){
				CWPlayer cwp = GameManager.getInstance().getPlayerToCWPlayer().get(p.getUniqueId());
				CWTeam cwt = cwp.getTeam();
				
				for(CWBuilding cwb : cwt.getCWBuildingList()){
					if(e.getClickedBlock().getLocation().distance(cwb.getLocation()) < 10 && cwp.getTeam().equals(cwb.getTeam())){
						Bukkit.broadcastMessage("building click registered for type: "+cwb.getCWBuildType()+" on team: "+cwb.getTeam().getColour());
						InventoryMenuManager.getInstance().procInv(cwb, cwp);
						
						
					} else Bukkit.broadcastMessage("click detected but too far away from building?");
				}
				
			}
		}
	}

}
