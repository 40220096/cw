package me.caedus.EventListeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;
import me.caedus.Manager.GameManager;

public class ItemPickUpEventListener implements Listener {
	
	@EventHandler
	public void onItemPickUpEvent(PlayerPickupItemEvent e){
		Player p = (Player) e.getPlayer();
		Material im = e.getItem().getItemStack().getType();
		
		CWPlayer cwp = GameManager.getInstance().getPlayerToCWPlayer().get(p.getUniqueId());
		CWTeam cwt = cwp.getTeam();
		if(im == Material.GOLD_INGOT){
			cwt.setTeamBalance(cwt.getTeamBalance() + 200*e.getItem().getItemStack().getAmount());
			cwp.setBalance(cwp.getBalance() + 50*e.getItem().getItemStack().getAmount());
			cwp.setTotalBalance(cwp.getTotalBalance() + 250*e.getItem().getItemStack().getAmount());
			e.getItem().remove();
		}
		Bukkit.broadcastMessage(""+cwp.getPlayer().getName()+" "+cwp.getTeam().getColour()+" "+cwt.getTeamBalance());
	}
	

}
