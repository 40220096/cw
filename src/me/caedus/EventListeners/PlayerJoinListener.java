package me.caedus.EventListeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;
import me.caedus.Manager.GameManager;

public class PlayerJoinListener implements Listener {
	
	//private ColonyWars plugin = ColonyWars.getInstance().getPlugin();
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent e){
		Player p = (Player) e.getPlayer();
		CWPlayer cwp = new CWPlayer(p);
		if(p.getName().equals("IceFrog")){			
			GameManager.getInstance().getPlayerToCWPlayer().put(p.getUniqueId(), cwp);
			cwp.setTeam(GameManager.getInstance().getBlueTeam());
			cwp.getTeam().addCWPlayer(cwp);
			Bukkit.broadcastMessage(""+cwp.getTeam().getColour()+" "+cwp.getTeam().getTeamBalance());
			
		} else {
			GameManager.getInstance().getPlayerToCWPlayer().put(p.getUniqueId(), cwp);
			cwp.setTeam(GameManager.getInstance().getRedTeam());
		}
		GameManager gm = GameManager.getInstance();
		Bukkit.broadcastMessage("Team: "+ GameManager.getInstance().getPlayerToCWPlayer().get(p.getUniqueId()).getTeam().getColour());
		CWPlayer cwpp = gm.getPlayerToCWPlayer().get(p.getUniqueId());
		Bukkit.broadcastMessage("name test: "+cwpp.getPlayer().getName()+" "+cwpp.getBalance());
		Bukkit.broadcastMessage("Team Balance: "+ gm.getPlayerToCWPlayer().get(p.getUniqueId()).getTeam().getTeamBalance());
		CWTeam cwpt = cwpp.getTeam();
		cwpt.setTeamBalance(cwpt.getTeamBalance()+200);

	}
}
