package me.caedus.ColonyWars;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.caedus.ColonyWarsBuildings.CWBuildType;
import me.caedus.ColonyWarsBuildings.CWBuilding;
import me.caedus.ColonyWarsEvents.CWBuildingBuildEvent;
import me.caedus.ColonyWarsEventsListener.CWBuildingBuildEventListener;
import me.caedus.ColonyWarsEventsListener.CWPlayerJoinTeamEventListener;
import me.caedus.EventListeners.BlockBreakEventListener;
import me.caedus.EventListeners.InventoryClickEventListener;
import me.caedus.EventListeners.ItemPickUpEventListener;
import me.caedus.EventListeners.PlayerInteractEventListner;
import me.caedus.EventListeners.PlayerJoinListener;
import me.caedus.Manager.GameManager;
import me.caedus.Manager.LocationManager;

public class ColonyWars extends JavaPlugin implements Listener{
	private static ColonyWars instance;
	
	
	public void onEnable(){
		instance = this;
		
		LocationManager lm = LocationManager.getInstance();
		
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new BlockBreakEventListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new ItemPickUpEventListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new CWBuildingBuildEventListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new CWPlayerJoinTeamEventListener(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerInteractEventListner(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new InventoryClickEventListener(), this);
		
		CWBuilding cwb = new CWBuilding(CWBuildType.TOWN_HALL, lm.getBlueTownHall(), GameManager.getInstance().getBlueTeam());
		
		CWBuildingBuildEvent e = new CWBuildingBuildEvent(cwb);
		Bukkit.getServer().getPluginManager().callEvent(e);
		
		for(CWBuilding cwbb : GameManager.getInstance().getBlueTeam().getCWBuildingList()){
			System.out.println(""+cwbb.getRank()+" "+cwbb.getLocation()+""+cwbb.getTeam().getColour()+" "+cwbb.getCWBuildType());
		}
	}

	public static ColonyWars getInstance(){
		return instance;
	}
}
