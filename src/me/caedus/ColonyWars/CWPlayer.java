package me.caedus.ColonyWars;

import org.bukkit.entity.Player;

public class CWPlayer {

	private CWTeam team;
	private Player p;
	private long playerBalance = 0;
	private boolean hasTeam = false;
	
	private int kills;
	private int deaths;
	private long totalBalance = 0;
	
	public CWPlayer(Player p){
		this.p = p;
	}
	
	public CWTeam getTeam(){
		return team;
	}
	public int getKills(){
		return this.kills;
	}
	public int getDeaths(){
		return this.deaths;
	}
	public void setKills(int kills){
		this.kills = kills;
	}
	public void setDeaths(int deaths){
		this.deaths = deaths;
	}
	
	public Player getPlayer(){
		return p;
	}
	
	public void setTeam(CWTeam t){
		this.team = t;
		this.hasTeam = true;
	}
	public boolean hasTeam(){
		return this.hasTeam;
	}
	
	public long getBalance(){
		return playerBalance;
	}
	public void setBalance(long balance){
		this.playerBalance = balance;
	}

	public long getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(long totalBalance) {
		this.totalBalance = totalBalance;
	}
	
}
