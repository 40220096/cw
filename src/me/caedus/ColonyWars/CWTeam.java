package me.caedus.ColonyWars;

import java.io.IOException;
import java.util.ArrayList;

import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.world.DataException;

import me.caedus.ColonyWarsBuildings.CWBuildType;
import me.caedus.ColonyWarsBuildings.CWBuilding;
import me.caedus.Manager.BuildManager;

public class CWTeam {
	
	private ArrayList<CWPlayer> teamPlayers = new ArrayList<>();
	private ArrayList<CWBuilding> teamBuildings  = new ArrayList<>();
	private String teamColour;
	private long teamBalance = 0;
	
	// Buildings
	@SuppressWarnings("unused")
	private CWBuilding townHall;
	
	private boolean hasFarm = false;
	private CWBuilding farm;
	
	private boolean hasLumberMill = false;
	private CWBuilding lumber_mill;
	
	private boolean hasBlackSmith = false;
	private CWBuilding blacksmith;
	
	private boolean hasMageTower = false;
	private CWBuilding magetower;
	
	private boolean hasChurch = false;
	private CWBuilding church;
	
	private boolean hasScoutTower = false;
	private CWBuilding scouttower;
	
	public void build(CWBuildType cwbt) throws MaxChangedBlocksException, DataException, IOException{
			BuildManager.getInstance().build(this, cwbt);	
	}
	
	public CWTeam(String teamColour){
		this.teamColour = teamColour;
	}
	public void setPlayers(ArrayList<CWPlayer> aL){
		this.teamPlayers = aL;	
	}	
	public void addCWPlayer(CWPlayer cwp){
		teamPlayers.add(cwp);
	}
	public void removeCWPlayer(CWPlayer cwp){
		if(teamPlayers.contains(cwp))
			teamPlayers.remove(cwp);
	}	
	public ArrayList<CWPlayer> getCWPlayers(){
		return teamPlayers;
	}	
	public String getColour(){
		return teamColour;
	}
	public long getTeamBalance(){
		return teamBalance;
	}
	public void setTeamBalance(long teamBalance){
		this.teamBalance = teamBalance;
	}
	public boolean hasFarm() {
		return hasFarm;
	}
	public CWBuilding getFarm() {
		return farm;
	}
	public void setFarm(CWBuilding farm) {
		this.farm = farm;
		this.hasFarm = true;
		if(!this.getCWBuildingList().contains(farm))
			this.getCWBuildingList().add(farm);
	}
	public boolean hasLumberMill() {
		return hasLumberMill;
	}
	public CWBuilding getLumber_mill() {
		return lumber_mill;
	}
	public void setLumber_mill(CWBuilding lumber_mill) {
		this.lumber_mill = lumber_mill;
		this.hasLumberMill = true;
		if(!this.getCWBuildingList().contains(lumber_mill))
			this.getCWBuildingList().add(lumber_mill);
	}
	public boolean hasBlackSmith() {
		return hasBlackSmith;
	}
	public CWBuilding getBlacksmith() {
		return blacksmith;
	}
	public void setBlacksmith(CWBuilding blacksmith) {
		this.blacksmith = blacksmith;
		this.hasBlackSmith = true;
		if(!this.getCWBuildingList().contains(blacksmith))
			this.getCWBuildingList().add(blacksmith);
	}
	public boolean hasMageTower() {
		return hasMageTower;
	}
	public CWBuilding getMagetower() {
		return magetower;
	}
	public void setMagetower(CWBuilding magetower) {
		this.magetower = magetower;
		this.hasMageTower = true;
		if(!this.getCWBuildingList().contains(magetower))
			this.getCWBuildingList().add(magetower);
	}
	public boolean hasChurch() {
		return hasChurch;
	}
	public CWBuilding getChurch() {
		return church;
	}
	public void setChurch(CWBuilding church) {
		this.church = church;
		this.hasChurch = true;
		if(!this.getCWBuildingList().contains(church))
			this.getCWBuildingList().add(church);
	}
	public boolean hasScoutTower() {
		return hasScoutTower;
	}
	public CWBuilding getScouttower() {
		return scouttower;
	}
	public void setScoutTower(CWBuilding scouttower) {
		this.scouttower = scouttower;
		this.hasScoutTower = true;
		if(!this.getCWBuildingList().contains(scouttower))
			this.getCWBuildingList().add(scouttower);
	}
	public void addCWBuildingList(CWBuilding cwb){
		if(!teamBuildings.contains(cwb)){
			teamBuildings.add(cwb);
		}
	}
	public ArrayList<CWBuilding> getCWBuildingList(){
		return this.teamBuildings;
	}
	public void setTownHall(CWBuilding cwb){
		this.townHall = cwb;
		if(!this.getCWBuildingList().contains(cwb))
			this.getCWBuildingList().add(cwb);
	}
	
}
