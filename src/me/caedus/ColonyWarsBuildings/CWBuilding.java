package me.caedus.ColonyWarsBuildings;

import org.bukkit.Location;

import me.caedus.ColonyWars.CWPlayer;
import me.caedus.ColonyWars.CWTeam;

public class CWBuilding {
	
	private CWPlayer cwp;
	private CWBuildType cwbt;
	private Location loc;
	private int buildingRank = 1;
	private CWTeam cwteam;
	private int buildingCost;
	
	public CWBuilding(CWBuildType cwbt, Location loc, CWTeam team){
		this.cwbt = cwbt;
		this.loc = loc;
		this.cwp = null;
		this.cwteam = team;
		
		calcCost(cwbt);
	}
	
	public CWBuilding(CWPlayer cwp, CWBuildType cwbt, Location loc){
		this.cwp = cwp;
		this.cwbt = cwbt;
		this.loc = loc;
		this.cwteam = cwp.getTeam();
		
		calcCost(cwbt);
	}	
	public CWPlayer getCWPlayer(){
		return this.cwp;		
	}
	public CWBuildType getCWBuildType(){
		return this.cwbt;
	}
	public Location getLocation(){
		return this.loc;
	}
	public int getRank(){
		return this.buildingRank;
	}
	public void setRank(int newRank){
		this.buildingRank = newRank;
	}
	public CWTeam getTeam(){
		return this.cwteam;
	}
	public int getBuildCost(){
		return this.buildingCost;
	}
	private void calcCost(CWBuildType cwbt){
		if(cwbt == CWBuildType.TOWN_HALL)
			this.buildingCost = 200;
	}
}
