package me.caedus.ColonyWarsBuildings;

public enum CWBuildType {
	TOWN_HALL, 
	FARM,
	LUMBER_MILL,
	BLACKSMITH,
	MAGE_TOWER,
	CHURCH,
	WALL,
	SCOUT_TOWER,
}

